
# Variant 14 Maltsev Konstantin MKIS14
def safe_cast_to_int(stringNum):
    try:
        return int(stringNum)
    except ValueError:
        print("Got unsupported symbol")


def safe_read_list(read_list, last_index):
    read_list.extend(list(map(lambda x: safe_cast_to_int(x), input().split())))

    i = 0
    while i < len(read_list):
        if read_list[i] is None:
            del read_list[i]
        i += 1

    if len(read_list) < last_index:
        print('Got list smaller than required')
        safe_read_list(read_list, last_index)

    del read_list[last_index:len(read_list)]


def init():
    task6()

# Это 14 варинт Константин Мальцев МКИС 14
# Тут конечно многое неоптимально, но я хотел немного поиздеваться над языком, так что получилось то, что получилось
def task1():
    list = []
    safe_read_list(list, 10)
    print("Got list: ", end='')
    print(list)
    for i in range(0, len(list), 2):
        try:
            list[i] *= 2
        except TypeError:
            print('Failed to double element with index: ' + str(i))
    print("List after operation: ", end='')
    print(list)


def task2():
    matrix_size = 7
    matrix = []
    for i in range(matrix_size):
        row = []
        safe_read_list(row, matrix_size)
        matrix.append(row)
    print("Got matrix:")
    for i in range(matrix_size):
        print(*matrix[i])

    for i in range(matrix_size, 1, -1):
        for j in range(i - 1):
            matrix[matrix_size - i][j] /= 2

    print("Matrix after operation:")
    for i in range(matrix_size):
        print(*matrix[i])

def task3():
    a = -1.0
    try:
        a = int(input())
    except ValueError:
        print("Input value is not a number")
        task3()
        return
    if(a <= 0):
        print("Square side cannot be either less than 0 or equal it")
        task3()
        return
    print("Square of square is: " + str(a * a))

#Ну тоже ничего так решение :)
def task4():
    word = input()
    print('Number of "MA" syllable is ' + str(len(word.split("ма")) -1))

def task5():
    print('Input a sentence')
    word = input()
    print("Now input the word index")
    word_index = int(input())
    word_list = word.split(" ")
    print("The word is: " + word_list[word_index -1])

def task6():
    print('Input a sentence')
    word = input()

    word_list = word.split(" ")

    sequence_count = 0
    letter_count = 0
    for sequence in word_list:
        sequence_count += 1
        letter_count += len(sequence)
    print(int(letter_count / sequence_count))

init()
