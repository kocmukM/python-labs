# Variant 14 Maltsev Konstantin MKIS14
# Это 14 варинт Константин Мальцев МКИС 14
def task1():
    sentence = input()
    words = sentence.split(' ')
    print(words[0] + ' ' + words[1] + ' ' + words[2] + ' вдохновляли птицы')


def task2_3():
    sentence = input()
    count = 0
    for letter in sentence:
        if letter.isupper():
            count += 1
    print('The number of capital letters is: ' + str(count))
    print('Without spaces: ' + sentence.replace(" ", ""))

#Или тут надо было прям алгоритм сортировки писать?
def task4_5():
    number_sentence = input()
    number_list = list(map(int, number_sentence.split(' ')))
    number_list.sort(key=None, reverse=True)
    print(number_list)
    print("Now input a word")
    word = input()
    print(''.join(sorted(word)))

def task6():
    word = input()
    print(sorted(list(set(word.lower().replace(" ", "")))))


task6()
